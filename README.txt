
-- SUMMARY --

This module allows for administrators to specify a set of key/value pairs
for use in node bodies.

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/dwieeb/1294972
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/1294972


-- REQUIREMENTS --

* None, but if you wish to have a button on your wysiwyg editor, you will
  need to install either the CKEditor or Wysiwyg module.


-- INSTALLATION --

* Install as usual, see 
  http://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

* Go to Administration � Help � Help topics � Data Dictionary to learn how
  to use Data Dictionary with your wysiwyg editor.


-- CONFIGURATION --

* Go to Administration � Configuration � Content authoring � Data Dictionary
  to add, remove or update your variables.

For a detailed configuration description, go to:
  Administration � Help � Help topics � Data Dictionary and look under 
  "Managing Your Variables"


-- CONTACT --

Current maintainers:
* Daniel Imhoff (dwieeb) - http://drupal.org/user/985186


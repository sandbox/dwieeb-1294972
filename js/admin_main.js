jQuery(document).ready(function($) {
  var count = 1;

  $('a.add-another').click(function() {
    ++count;

    $('table tr.new-variable').clone().removeClass('new-variable').insertBefore('table tbody tr:last-child')
      .find('#edit-add-new-key-1').val(undefined).attr({id: 'edit-add-new-key-' + count, name: 'add_new_key_' + count}).end()
      .find('#edit-add-new-value-1').val(undefined).attr({id: 'edit-add-new-value-' + count, name: 'add_new_value_' + count}).end()
      .hide().fadeIn();

    return false;
  });
});

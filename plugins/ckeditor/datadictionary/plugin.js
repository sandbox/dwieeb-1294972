(function($) {
  CKEDITOR.plugins.add('datadictionary', {
    init: function(editor, pluginPath) {
      var dictionary_variables = [];

      // Ajax call to grab the data dictionary variables from the database.
      $.getJSON(Drupal.settings.basePath + 'datadictionary/variables', function(data) {
        dictionary_variables = data;
      });

      // Register a dialog box with CKEditor.
      CKEDITOR.dialog.add('datadictionary_dialog', function(editor) {
        return {
          title: Drupal.t('Add a data dictionary variable'),
          minWidth: 250,
          minHeight: 50,
          contents: [
            {
              id: 'info',
              label: Drupal.t('Add a data dictionary variable'),
              title: Drupal.t('Add a data dictionary variable'),
              elements: [
                {
                  id: 'datadictionary',
                  type: 'select',
                  items: dictionary_variables,
                  label: Drupal.t('Select a variable to insert:')
                }
              ]
            }
          ],
          onOk: function() {
            var editor = this.getParentEditor();
            var content = this.getValueOf('info', 'datadictionary');
            if (content.length > 0) {
              var realElement = CKEDITOR.dom.element.createFromHtml('<span class="datadictionary-value"></span>');
              realElement.setHtml('~' + content + '~');
              editor.insertElement(realElement);
            }
          }
        };
      });

      // Register a command with CKeditor to launch the dialog box.
      editor.addCommand('DataDictionary', new CKEDITOR.dialogCommand('datadictionary_dialog'));

      // Add a button to the CKeditor that executes a CKeditor command.
      editor.ui.addButton('DataDictionary', {
        label: Drupal.t('Add a data dictionary variable'),
        command: 'DataDictionary',
        icon: this.path + 'images/datadictionary.png'
      });
    }
  });
})(jQuery);

(function() {
  tinymce.create('tinymce.plugins.DataDictionaryPlugin', {
    /**
     * Initialize the plugin, executed after the plugin has been created.
     *
     * This call is done before the editor instance has finished it's
     * initialization so use the onInit event of the editor instance to
     * intercept that event.
     *
     * @param ed
     *   The tinymce.Editor instance the plugin is initialized in.
     * @param url
     *   The absolute URL of the plugin location.
     */
    init: function(ed, url) {
      ed.addCommand('DataDictionary', function() {
        ed.windowManager.open({
          file: url + '/datadictionary.html',
          width: 330,
          height: 100,
          inline: 1
        });
      });

      // Register DataDictionary button.
      // The first argument should match the <key> used in 
      // @plugins[<plugin name>]["buttons"][<key>] in hook_wysiwyg_plugin()
      ed.addButton('datadictionary', {
        title: Drupal.t('Add a data dictionary variable'),
        cmd: 'DataDictionary', // This should match the string in ed.addCommand above
        image: url + '/images/datadictionary.png'
      });
    },

    /**
     * Return information about the plugin as a name/value array.
     */
    getInfo: function() {
      return {
        longname: 'TinyMCE Data Dictionary',
        author: 'Daniel Imhoff',
        authorurl: 'http://www.danielimhoff.com',
        infourl: 'http://www.danielimhoff.com/drupal-modules/data-dictionary/',
        version: "1"
      };
    }
  });

  // Register plugin.
  tinymce.PluginManager.add('datadictionary', tinymce.plugins.DataDictionaryPlugin);
})();

<?php
/**
 * @file
 * Administration page for the Data Dictionary module.
 *
 * @author Daniel Imhoff
 */

/**
 * This form is displayed on the admin config page. It is used for managing data dictionary variables.
 */
function _datadictionary_admin_main_form($form, &$form_state) {
  global $base_path;

  $form = $variables = array();

  $form['#attached']['css'][] = drupal_get_path('module', 'datadictionary') . '/css/admin_main.css';
  $form['#attached']['js'][] = drupal_get_path('module', 'datadictionary') . '/js/admin_main.js';
  $form['#attached']['js'][] = $base_path . 'misc/tableheader.js';

  $results = db_select('datadictionary', 'd', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('d')
    ->execute()
    ->fetchAll();

  foreach ($results as $variable) {
    $variables[$variable['name']] = $variable['value'];
  }

  if (is_array($variables)) {
    ksort($variables);
  }

  $form[] = array(
    '#markup' => '<table id="data-dictionary" class="sticky-enabled">' .
                 '<thead>' .
                 '<tr>' .
                 '<th class="checkbox">' . t('Delete') . '</th>' .
                 '<th>' . t('Key') . '</th>' .
                 '<th>' . t('Value') . '</th>' .
                 '</tr>' .
                 '</thead>' .
                 '<tbody>' .
                 '<tr>' .
                 '<th colspan="3">' . t('Existing Variables') . '</th>' .
                 '</tr>',
  );

  if (empty($variables)) {
    $form[] = array(
      '#markup' => '<tr>' .
                   '<td colspan="3"><p style="text-align:center;">' . t('You have no variables! Add some by using the form below.') . '</p></td>' .
                   '</tr>',
    );
  }
  else {
    $counter = 0;

    foreach ($variables as $key => $value) {
      $form[] = array(
        '#markup' => '<tr class="' . (++$counter % 2 == 0 ? "even" : "odd") . '">',
      );

      $form['del_' . $key] = array(
        '#type' => 'checkbox',
        '#prefix' => '<td class="checkbox">',
        '#suffix' => '</td>',
      );

      $form[] = array(
        '#markup' => '<td>' . $key . '</td>',
      );

      $form['var_' . $key] = array(
        '#type' => 'textfield',
        '#default_value' => $value,
        '#attributes' => array(
          'style' => 'width:99%;',
        ),
        '#prefix' => '<td>',
        '#suffix' => '</td>',
      );

      $form[] = array(
        '#markup' => '</tr>',
      );
    }
  }

  $form[] = array(
    '#markup' => '<tr>' .
                 '<th colspan="3">' . t('Add New Variable(s)') . '</th>' .
                 '</tr>' .
                 '<tr class="new-variable odd">' .
                 '<td>&nbsp;</td>',
  );

  $form['add_new_key_1'] = array(
    '#type' => 'textfield',
    '#attributes' => array(
      'style' => 'width:99%;',
    ),
    '#maxlength' => 64,
    '#prefix' => '<td>',
    '#suffix' => '</td>',
  );

  $form['add_new_value_1'] = array(
    '#type' => 'textfield',
    '#attributes' => array(
      'style' => 'width:99%;',
    ),
    '#maxlength' => 256,
    '#prefix' => '<td>',
    '#suffix' => '</td>',
  );

  $form[] = array(
    '#markup' => '</tr>' .
                 '<tr id="add-new-variable">' .
                 '<td colspan="3"><a href="#" class="add-another">Add new variable</a></td>' .
                 '</tr>' .
                 '</tbody>' .
                 '</table>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

/**
 * The submit handler for the admin config page form.
 */
function _datadictionary_admin_main_form_submit($form, &$form_state) {
  $existing_variables = array();
  $num_deleted = $num_added = 0;

  $results = db_select('datadictionary', 'd', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('d')
    ->execute()
    ->fetchAll();

  foreach ($results as $variable) {
    $existing_variables[$variable['name']] = $variable['value'];
  }

  foreach ($form_state['input'] as $key => $value) {
    // Update variables
    if (strpos($key, 'var_') === 0) {
      $key = drupal_substr($key, 4);
      if (!empty($key) && !empty($value) && isset($existing_variables[$key]) && $existing_variables[$key] != $value) {
        db_update('datadictionary')
          ->fields(array(
            'value' => $value,
          ))
          ->condition('name', $key)
          ->execute();
      }
    }
    // Delete variables
    elseif (strpos($key, 'del_') === 0) {
      if (!empty($value)) {
        db_delete('datadictionary')
          ->condition('name', drupal_substr($key, 4))
          ->execute();
        ++$num_deleted;
      }
    }
    // Add variables
    elseif (strpos($key, 'add_new_key_') === 0) {
      $id = drupal_substr($key, 12);
      $key = _datadictionary_sanitize($form_state['input']['add_new_key_' . $id]);
      $value = $form_state['input']['add_new_value_' . $id];
      if (!empty($key) && !empty($value)) {
        if (isset($existing_variables[$key])) {
          drupal_set_message(t('Variable with key "@key" already exists. Please choose a different key for your new variable.', array('@key' => $key)), 'error');
        }
        else {
          db_insert('datadictionary')
            ->fields(array(
              'name' => $key,
              'value' => $value,
            ))
            ->execute();
          ++$num_added;
        }
      }
    }
  }

  drupal_set_message(t('Variables updated.') .
                     ' ' . ($num_added ? t('@num_added added to data dictionary.', array('@num_added' => format_plural($num_added, '1 new variable', '@count new variables'))) : '') .
                     ' ' . ($num_deleted ? t('@num_deleted removed from data dictionary.', array('@num_deleted' => format_plural($num_deleted, '1 variable', '@count variables'))) : ''));
}